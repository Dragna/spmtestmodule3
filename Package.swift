// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SPMTestModule3",
    platforms: [.iOS(.v12)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SPMTestModule3",
            targets: ["SPMTestModule3"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", .upToNextMajor(from: "5.1.1")),
        .package(url: "https://github.com/RxSwiftCommunity/RxOptional", .upToNextMajor(from: "4.0.0")),
        .package(url: "https://github.com/Swinject/Swinject", .upToNextMajor(from: "2.7.1")),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver", .upToNextMajor(from: "1.9.2")),
        .package(name: "SPMTestModule2",url: "https://gitlab.com/Dragna/spmtestmodule2", .branch("master")),
        .package(name: "SPMTestModule1", url: "https://gitlab.com/Dragna/spmtestmodule1", .branch("master")),
//        .package(name: "AWSSDK", url: "https://gitlab.com/Dragna/aws-sdk-ios-spmtest", .branch("master"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SPMTestModule3",
            dependencies: [.product(name: "RxSwift", package: "RxSwift"),
                           "RxOptional",
                           .product(name: "SPMTestModule1", package: "SPMTestModule1"),
                           "SwiftyBeaver",
                           "SPMTestModule2",
                           "Swinject",
//                           .product(name: "AWSCore", package: "AWSSDK"),
//                           .product(name: "AWSIoT", package: "AWSSDK"),
//                           .product(name: "AWSCognitoIdentityProviderASF", package: "AWSSDK"),
//                           .product(name: "AWSCognitoIdentityProvider", package: "AWSSDK"),
//                           .product(name: "AWSCognitoAuth", package: "AWSSDK"),
//                           .product(name: "AWSCognito", package: "AWSSDK"),
                            ],
            path: "Sources/SPMTestModule3"),
    ]
)
